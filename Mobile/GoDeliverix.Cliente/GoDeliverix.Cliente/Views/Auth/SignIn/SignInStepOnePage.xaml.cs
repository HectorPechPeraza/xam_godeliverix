﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GoDeliverix.Cliente.Views.Auth.SignIn
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignInStepOnePage : ContentPage
    {
        public SignInStepOnePage()
        {
            InitializeComponent();
        }
    }
}