﻿using Xamarin.Forms.Xaml;

namespace GoDeliverix.Cliente.Views.Dialogs
{
    /// <summary>
    /// Dialog to filter products on home page
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductFilterDialog 
    {
        public ProductFilterDialog()
        {
            InitializeComponent();
        }
    }
}