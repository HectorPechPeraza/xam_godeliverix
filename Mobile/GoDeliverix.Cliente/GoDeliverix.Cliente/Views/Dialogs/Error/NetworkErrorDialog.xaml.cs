﻿using Xamarin.Forms.Xaml;

namespace GoDeliverix.Cliente.Views.Dialogs.Error
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NetworkErrorDialog 
    {
        public NetworkErrorDialog()
        {
            InitializeComponent();
        }
    }
}