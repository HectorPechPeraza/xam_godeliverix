﻿using Xamarin.Forms;
using Xamarin.Essentials;
using System;
using GoDeliverix.Cliente.Services.Common;
using GoDeliverix.Cliente.Utils;
using GoDeliverix.Cliente.Models.Common;
using System.Collections;
using GoDeliverix.Cliente.Models.Pais;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using Xamarin.Forms.Maps;
using System.Threading.Tasks;
using GoDeliverix.Cliente.Helpers;
using GoDeliverix.Cliente.Services;
using GoDeliverix.Cliente.Models.LocalStorage;
using GoDeliverix.Cliente.ViewModels.Location;

namespace GoDeliverix.Cliente.Views.Location
{
    public partial class ChooseLocationPage : ContentPage
    {
        #region Properties
        private IEnumerable<Pais> Paises { get; set; }
        #endregion

        #region Services
        private LocationService _locationService;
        #endregion

        public ChooseLocationPage()
        {
            InitializeComponent();

            this._locationService = new LocationService();

            if (Settings.DireccionSession != null)
            {
                var address = Settings.DireccionSession;
                this.CreateMapPin(address.Latitude, address.Longitude);
                lCountry.Text = address.Pais;
                lState.Text = $"{address.Estado}, {address.Municipio}";
                lFullAddress.Text = address.Direccion;
            }
        }

        #region Events
        private async void MyLocation_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                aiLoader.IsRunning = true;

                var location = await Geolocation.GetLocationAsync();

                if (location != null)
                {
                    var viewModel = BindingContext as ChooseLocationPageViewModel;
                    var address = await this._locationService.GetAddressByGeolocation(location.Latitude, location.Longitude);
                    
                    if (address == null)
                    {
                        this.CreateMapPin(location.Latitude, location.Longitude, "No disponible");

                        lCountry.Text = "-";
                        lState.Text = "-";
                        lFullAddress.Text = "-";

                        viewModel.Direccion = null;

                        this.DisplayMessageDialog("Servicio no disponible en la ubicacion seleccionada");
                        aiLoader.IsRunning = false;
                        return;
                    }

                    this.CreateMapPin(location.Latitude, location.Longitude);

                    lCountry.Text = address.Pais;
                    lState.Text = $"{address.Estado}, {address.Municipio}";
                    lFullAddress.Text = address.Direccion;

                    address.Latitude = location.Latitude;
                    address.Longitude = location.Longitude;

                    viewModel.Direccion = address;
                }

                aiLoader.IsRunning = false;
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                this.DisplayMessageDialog(fnsEx.Message);
                aiLoader.IsRunning = false;
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
                this.DisplayMessageDialog(fneEx.Message);
                aiLoader.IsRunning = false;
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
                this.DisplayMessageDialog(pEx.Message);
                aiLoader.IsRunning = false;
            }
            catch (Exception ex)
            {
                // Unable to get location
                this.DisplayMessageDialog(ex.Message);
                aiLoader.IsRunning = false;
            }
        }

        private async void mLocationMap_MapClicked(object sender, MapClickedEventArgs e)
        {
            try
            {
                aiLoader.IsRunning = true;

                var viewModel = BindingContext as ChooseLocationPageViewModel;
                var address = await this._locationService.GetAddressByGeolocation(e.Position.Latitude, e.Position.Longitude);

                if (address == null)
                {
                    this.CreateMapPin(e.Position.Latitude, e.Position.Longitude, "No disponible");

                    lCountry.Text = "-";
                    lState.Text = "-";
                    lFullAddress.Text = "-";

                    viewModel.Direccion = null;

                    this.DisplayMessageDialog("Servicio no disponible en la ubicacion seleccionada");
                    aiLoader.IsRunning = false;
                    return;
                }

                this.CreateMapPin(e.Position.Latitude, e.Position.Longitude);

                lCountry.Text = address.Pais;
                lState.Text = $"{address.Estado}, {address.Municipio}";
                lFullAddress.Text = address.Direccion;

                address.Latitude = e.Position.Latitude;
                address.Longitude = e.Position.Longitude;

                viewModel.Direccion = address;

                aiLoader.IsRunning = false;
            }
            catch (Exception ex)
            {
                this.DisplayMessageDialog(ex.Message);
            }
        }
        #endregion

        #region Requests
        #endregion

        #region Implementation
        private async void DisplayMessageDialog(string message, string title = "")
        {
            await DisplayAlert(title, message, "Aceptar");
        }

        private void CreateMapPin(double latitude, double longitude, string tag = "Ubicacion de entrega")
        {
            Position myPosition = new Position(latitude, longitude);
            MapSpan mapSpan = new MapSpan(myPosition, 0.002, 0.002);

            mLocationMap.MoveToRegion(mapSpan);
            mLocationMap.Pins.Clear();
            mLocationMap.Pins.Add(new Pin() { Label = tag, Type = PinType.Place, Position = myPosition });
        }
        #endregion
    }
}
