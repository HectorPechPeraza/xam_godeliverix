﻿using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

namespace GoDeliverix.Cliente.Views.Layout
{
    public partial class NavigationTabbedPage : Xamarin.Forms.TabbedPage
    {
        public NavigationTabbedPage()
        {
            InitializeComponent();
            On<Xamarin.Forms.PlatformConfiguration.Android>().SetToolbarPlacement(ToolbarPlacement.Bottom);
        }

    }
}
