﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoDeliverix.Cliente.Utils
{
    /// <summary>
    /// see => https://andreinitescu.github.io/IconFont2Code/
    /// </summary>
    public static class IconFont
    {
        public const string Account = "\U000f0004";
        public const string Home = "\U000f02dc"; 
        public const string BookOpenVariant = "\U000f00be";
        public const string History = "\U000f02da";
        public const string Wallet = "\U000f0584";
        public const string Logout = "\U000f0343";
    }
}
