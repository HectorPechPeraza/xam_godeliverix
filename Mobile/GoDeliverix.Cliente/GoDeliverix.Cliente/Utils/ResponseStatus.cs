﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoDeliverix.Cliente.Utils
{
    /// <summary>
    /// To identify the status of the response
    /// </summary>
    public enum ResponseStatus
    {
        Success,
        NetworkError,
        Failed
    }
}
