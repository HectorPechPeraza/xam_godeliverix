﻿using GoDeliverix.Cliente.Helpers;
using GoDeliverix.Cliente.Models.Products;
using GoDeliverix.Cliente.Services.Dependency.Abstract;
using GoDeliverix.Cliente.ViewModels.Common;
using GoDeliverix.Cliente.Views.Auth.Login;
using GoDeliverix.Cliente.Views.Dialogs;
using GoDeliverix.Cliente.Views.Location;
using GoDeliverix.Cliente.Views.Product;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.ObjectModel;

namespace GoDeliverix.Cliente.ViewModels.Home
{
    public class HomePageViewModel : BaseSessionViewModel
    {
        #region Properties
        private ObservableCollection<ProductGrid> _products;
        public ObservableCollection<ProductGrid> Products
        {
            get
            {
                return this._products;
            }
            set
            {
                SetProperty(ref this._products, value);
            }
        }

        private ProductGrid _selectedProduct;
        public ProductGrid SelectedProduct
        {
            get
            {
                return this._selectedProduct;
            }
            set
            {
                SetProperty(ref this._selectedProduct, value);
            }
        }

        private string header;
        public string Header
        {
            get
            {
                return this.header;
            }
            set
            {
                SetProperty(ref this.header, value);
            }
        }
        #endregion

        public HomePageViewModel(
            INavigationService navigationService,
            IDialogService dialogService,
            IApplicationInstance applicationInstance) : base(navigationService, dialogService, applicationInstance)
        {
            this._navigationService = navigationService;

            // Initialize Commands
            this.ItemSelectedCommand = new DelegateCommand<string>(this.GoToProductDetail);
            this.LocationClickedCommand = new DelegateCommand(OnLocationClicked);
            this.ShowFilterClickedCommand = new DelegateCommand(OnShowFilterClicked);

            this.Header = $"Entregar: {Settings.DireccionSession.Direccion}";

            // Initialize Properties
            this.Products = new ObservableCollection<ProductGrid>();
            int ramdom = new Random().Next(5, 15);
            for (int i = 0; i < 5; i++)
            {
                this.Products.Add(new ProductGrid()
                {
                    Uid = Guid.NewGuid(),
                    Company = $"Tortas Alejandra",
                    Description = $"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat. Duis aute irure dolor in reprehenderit in voluptate velit essecillum oloreeu fugiat nulla pariatur. Excepteur sint occaecat cupidatat nonproident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                    Price = i * 10,
                    Title = $"Product {i.ToString("N2")}"
                });
            }
        }

        #region Command
        public DelegateCommand<string> ItemSelectedCommand { get; }

        public DelegateCommand LocationClickedCommand { get; }

        public DelegateCommand ShowFilterClickedCommand { get; }
        #endregion

        #region Implementation
        private async void OnLoginClicked()
        {
            await this._navigationService.NavigateAsync($"{nameof(LoginPage)}");
        }

        private async void GoToProductDetail(string Uid)
        {
            NavigationParameters parameters = new NavigationParameters();
            parameters.Add("Uid", Uid);
            await this._navigationService.NavigateAsync($"{nameof(ProductDetailPage)}", parameters);
        }

        private async void OnLocationClicked()
        {
            var current = this._navigationService.GetNavigationUriPath();
            var nav = await this._navigationService.NavigateAsync($"{nameof(ChooseLocationPage)}", useModalNavigation: true);
        }

        private void OnShowFilterClicked()
        {
            this._dialogService.ShowDialog($"{nameof(ProductFilterDialog)}");
        }
        #endregion

    }
}
