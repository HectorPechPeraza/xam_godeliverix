﻿using GoDeliverix.Cliente.Helpers;
using GoDeliverix.Cliente.Services;
using GoDeliverix.Cliente.Views.Dialogs.Error;
using GoDeliverix.Cliente.Views.Layout;
using GoDeliverix.Cliente.Views.Layout.Error;
using GoDeliverix.Cliente.Views.Welcome;
using Prism.AppModel;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services.Dialogs;
using Xamarin.Essentials;

namespace GoDeliverix.Cliente.ViewModels.Home
{
    public class SplashPageViewModel : BindableBase, IPageLifecycleAware
    {
        #region Properties
        private bool _loading;
        public bool Loading
        {
            get
            {
                return this._loading;
            }
            set
            {
                SetProperty(ref this._loading, value);
            }
        }

        private string _progressText;
        public string ProgressText
        {
            get { return _progressText; }
            set { SetProperty(ref _progressText, value); }
        }

        #endregion

        #region Internal
        protected INavigationService _navigationService;
        protected IDialogService _dialogService;
        #endregion

        #region Services
        private LocationService _locationService;
        #endregion

        public SplashPageViewModel(INavigationService navigationService, IDialogService dialogService)
        {
            // Initialize Instances
            this._navigationService = navigationService;
            this._dialogService = dialogService;

            // Initialize Services
            this._locationService = new LocationService();

            // Display Loader
            this.Loading = true;
            this.ProgressText = "Bienvenido!";

        }

        #region Methods
        private async void ValidateApplicationSession()
        {
            var current = Connectivity.NetworkAccess;
            if (current != NetworkAccess.Internet)
            {
                this._dialogService.ShowDialog($"{nameof(NetworkErrorDialog)}", this.CheckAfterValidatingInternetAccess);
                return;
            }

            this.CheckAfterValidatingInternetAccess(null);
        }

        private async void CheckAfterValidatingInternetAccess(IDialogResult dialogResult)
        {
            bool available = await this._locationService.ValidateServiceAvailability();
            if (!available)
            {
                await this._navigationService.NavigateAsync($"/{nameof(NotAvailablePage)}");
                return;
            }

            // Validate if user is logged and unregistered address
            if (Settings.UserSession == null && Settings.DireccionSession == null)
            {
                await this._navigationService.NavigateAsync($"/{nameof(WelcomePage)}");
            }
            // Validate registered address
            else if (Settings.DireccionSession != null)
            {
                await this._navigationService.NavigateAsync($"/{nameof(NavigationTabbedPage)}");
            }
            else
            {

            }
        }
        #endregion

        #region Page Lifecycle
        public void OnAppearing()
        {
            this.ValidateApplicationSession();
        }
        public void OnDisappearing()
        {

        }
        #endregion
    }
}
