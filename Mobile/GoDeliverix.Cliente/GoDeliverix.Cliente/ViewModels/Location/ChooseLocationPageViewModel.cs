﻿using GoDeliverix.Cliente.Helpers;
using GoDeliverix.Cliente.Models.Direccion;
using GoDeliverix.Cliente.Models.LocalStorage;
using GoDeliverix.Cliente.Views.Layout;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GoDeliverix.Cliente.ViewModels.Location
{
    public class ChooseLocationPageViewModel : BindableBase
    {
        #region Properties
        /// <summary>
        /// No (Binding)
        /// </summary>
        public DireccionCatalogo Direccion { get; set; }
        #endregion

        #region Internal
        private INavigationService _navigationService;
        private IPageDialogService _dialogService;
        #endregion

        public ChooseLocationPageViewModel(INavigationService navigationService, IPageDialogService dialogService)
        {
            // Initialize Dependencies
            this._navigationService = navigationService;
            this._dialogService = dialogService;

            // Initialize
            this.NextCommand = new DelegateCommand(this.NextClick);

            if (Settings.DireccionSession == null)
            {
                this.Direccion = null;
            } else
            {
                var memoryAddress = Settings.DireccionSession;
                this.Direccion = new DireccionCatalogo()
                {
                    Direccion = memoryAddress.Direccion,
                    Latitude = memoryAddress.Latitude,
                    Longitude = memoryAddress.Longitude,
                    UidCiudad = memoryAddress.UidCiudad,
                    Ciudad = memoryAddress.Ciudad,
                    UidColonia = memoryAddress.UidColonia,
                    Colonia = memoryAddress.Colonia,
                    UidEstado = memoryAddress.UidPais,
                    Estado = memoryAddress.Estado,
                    UidMunicipio = memoryAddress.UidMunicipio,
                    Municipio = memoryAddress.Municipio,
                    UidPais = memoryAddress.UidPais,
                    Pais = memoryAddress.Pais
                };
            }
        }

        #region Command
        public DelegateCommand NextCommand { get; }
        #endregion

        #region Implementation
        private async void NextClick()
        {
            if (Direccion == null)
            {
                await _dialogService.DisplayAlertAsync("", "Es necesario seleccionar una ubicacion valida para continuar", "Ok");
                return;
            }

            Settings.DireccionSession = new DireccionSession()
            {
                Direccion = this.Direccion.Direccion,
                Latitude = this.Direccion.Latitude,
                Longitude = this.Direccion.Longitude,
                Uid = Guid.Empty,
                UidCiudad = this.Direccion.UidCiudad,
                Ciudad = this.Direccion.Ciudad,
                UidColonia = this.Direccion.UidColonia,
                Colonia = this.Direccion.Colonia,
                UidEstado = this.Direccion.UidPais,
                Estado = this.Direccion.Estado,
                UidMunicipio = this.Direccion.UidMunicipio,
                Municipio = this.Direccion.Municipio,
                UidPais = this.Direccion.UidPais,
                Pais = this.Direccion.Pais
            };

            await this._navigationService.NavigateAsync($"/{nameof(NavigationTabbedPage)}");
        }
        #endregion
    }
}
