﻿using GoDeliverix.Cliente.Views.Auth.Login;
using GoDeliverix.Cliente.Views.Auth.SignIn;
using GoDeliverix.Cliente.Views.Location;
using Prism.AppModel;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GoDeliverix.Cliente.ViewModels.Welcome
{
    public class WelcomePageViewModel : BindableBase, IPageLifecycleAware
    {
        #region Properties

        #endregion

        #region Internal
        protected INavigationService _navigationService;
        #endregion

        public WelcomePageViewModel(INavigationService navigationService)
        {
            // Initialize Instances
            this._navigationService = navigationService;

            // Initialize Commands
            this.LoginCommand = new DelegateCommand(this.NavigateLoginPage);
            this.SignInCommand = new DelegateCommand(this.NavigateSignInPage);
            this.WithoutAccountCommand = new DelegateCommand(this.NavigateChooseLocationPage);
        }

        #region Command
        public DelegateCommand LoginCommand { get; }
        public DelegateCommand SignInCommand { get; }
        public DelegateCommand WithoutAccountCommand { get; }
        #endregion

        #region Implementation
        private async void NavigateLoginPage()
        {
            await this._navigationService.NavigateAsync($"{nameof(LoginPage)}");
        }
        private async void NavigateSignInPage()
        {
            await this._navigationService.NavigateAsync($"{nameof(SignInStepOnePage)}");
        }
        private async void NavigateChooseLocationPage()
        {
            await this._navigationService.NavigateAsync($"{nameof(ChooseLocationPage)}");
        }
        #endregion

        #region Page Lifecycle
        public void OnAppearing()
        {

        }
        public void OnDisappearing()
        {

        }
        #endregion
    }
}
