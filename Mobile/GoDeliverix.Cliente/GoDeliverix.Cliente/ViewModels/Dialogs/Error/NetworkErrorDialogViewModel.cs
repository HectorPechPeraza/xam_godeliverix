﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace GoDeliverix.Cliente.ViewModels.Dialogs.Error
{
    public class NetworkErrorDialogViewModel : BindableBase, IDialogAware
    {

        #region Properties
        private bool loading;
        public bool Loading
        {
            get
            {
                return this.loading;
            }
            set
            {
                SetProperty(ref this.loading, value);
            }
        }
        #endregion

        public event Action<IDialogParameters> RequestClose;

        public bool CanCloseDialog() => true;

        public void OnDialogClosed()
        {
            // TODO
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            
        }

        public NetworkErrorDialogViewModel()
        {
            this.Loading = false;
            this.ValidateNetworkAccess = new DelegateCommand(this.ValidateNetworkAccessClick);
        }

        #region Commands
        public DelegateCommand ValidateNetworkAccess { get; }
        #endregion

        #region Implementation
        private async void ValidateNetworkAccessClick()
        {           
            this.Loading = true;

            await Task.Delay(1000);

            var current = Connectivity.NetworkAccess;

            switch (current)
            {
                case NetworkAccess.Internet:
                    // Connected to internet
                    this.RequestClose(null);
                    break;
                case NetworkAccess.Local:
                    // Only local network access
                    break;
                case NetworkAccess.ConstrainedInternet:
                    // Connected, but limited internet access such as behind a network login page
                    break;
                case NetworkAccess.None:
                    // No internet available
                    break;
                case NetworkAccess.Unknown:
                    // Internet access is unknown
                    break;
            }

            this.Loading = false;
        }
        #endregion
    }
}
