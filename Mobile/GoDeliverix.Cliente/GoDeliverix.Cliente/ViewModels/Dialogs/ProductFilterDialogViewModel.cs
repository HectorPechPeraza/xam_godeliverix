﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Text;

namespace GoDeliverix.Cliente.ViewModels.Dialogs
{
    public class ProductFilterDialogViewModel : BindableBase, IDialogAware
    {
        #region Properties

        #endregion

        #region Internal

        public event Action<IDialogParameters> RequestClose;

        public bool CanCloseDialog() => true;

        public void OnDialogClosed() { }

        public void OnDialogOpened(IDialogParameters parameters) { }
        #endregion

        public ProductFilterDialogViewModel()
        {
            this.ApplyFilterCommand = new DelegateCommand(ApplyFilter);
            this.CloseCommand = new DelegateCommand(() => this.RequestClose(null));
        }

        #region Command
        public DelegateCommand CloseCommand { get; }
        public DelegateCommand ApplyFilterCommand { get; set; }
        #endregion

        #region Implementation
        private void ApplyFilter()
        {
            this.RequestClose(null);
        }
        #endregion
    }
}
