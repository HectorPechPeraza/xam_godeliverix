﻿using GoDeliverix.Cliente.Models.Navigation;
using GoDeliverix.Cliente.Services.Shared;
using GoDeliverix.Cliente.Utils;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using System.Collections.Generic;

namespace GoDeliverix.Cliente.ViewModels.Auth
{
    public class LoginPageViewModel : BindableBase
    {
        #region Properties
        private string _username;
        public string Username
        {
            get
            {
                return this._username;
            }
            set
            {
                SetProperty(ref this._username, value);
            }
        }

        private string _password;
        public string Password
        {
            get
            {
                return this._password;
            }
            set
            {
                SetProperty(ref this._password, value);
            }
        }

        private string _rememberPassword;
        public string RememberPassword
        {
            get
            {
                return this._rememberPassword;
            }
            set
            {
                SetProperty(ref this._rememberPassword, value);
            }
        }
        #endregion

        #region Internal
        protected INavigationService NavigationService;
        protected IEventAggregator _ea;
        #endregion

        public LoginPageViewModel(INavigationService navigationService, IEventAggregator ea)
        {
            this.NavigationService = navigationService;

            this._ea = ea;

            this.LoginCommand = new DelegateCommand(this.OnLoginClicked);
        }

        #region Command
        public DelegateCommand LoginCommand { get; }
        #endregion

        #region Implementation
        public async void OnLoginClicked()
        {
            this.SetLoggedUserNavigation();
        }

        private void SetLoggedUserNavigation()
        {
            List<NavigationItem> nav = new List<NavigationItem>();
            nav.Add(new NavigationItem() { Icon = IconFont.Home, PageName = "", Title = "Inicio" });
            nav.Add(new NavigationItem() { Icon = IconFont.BookOpenVariant, PageName = "", Title = "Monedero" });
            nav.Add(new NavigationItem() { Icon = IconFont.History, PageName = "", Title = "Historial" });
            nav.Add(new NavigationItem() { Icon = IconFont.Account, PageName = "", Title = "Cuenta" });
            nav.Add(new NavigationItem() { Icon = IconFont.Logout, PageName = "", Title = "Salir" });

            this._ea.GetEvent<NavigationServiceEvent>().Publish(nav);
        }
        #endregion
    }
}
