﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GoDeliverix.Cliente.ViewModels.Product
{
    public class ProductDetailPageViewModel : BindableBase, INavigationAware
    {
        #region Properties
        protected Guid Uid { get; set; }
        #endregion

        #region Internal
        private INavigationService _navigationService;
        #endregion

        public ProductDetailPageViewModel(INavigationService navigationService)
        {
            this._navigationService = navigationService;
        }

        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            // throw new NotImplementedException();
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("Uid"))
            {
                this.Uid = parameters.GetValue<Guid>("Uid");
            }
        }
    }
}
