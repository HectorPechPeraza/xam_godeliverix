﻿using GoDeliverix.Cliente.Helpers;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GoDeliverix.Cliente.ViewModels.Account
{
    public class AccountPageViewModel : BindableBase
    {
        #region Properties
        private bool _isLogged;
        public bool IsLogged
        {
            get => this._isLogged;
            set => SetProperty(ref this._isLogged, value);
        }

        public bool DisplayLoginDialog
        {
            get => !this._isLogged;
        }
        #endregion

        public AccountPageViewModel()
        {
            this.IsLogged = Settings.UserGuid == Guid.Empty ? false : true;
        }
    }
}
