﻿using GoDeliverix.Cliente.Services.Dependency.Abstract;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services.Dialogs;

namespace GoDeliverix.Cliente.ViewModels.Common
{
    public class BaseSessionViewModel : BindableBase
    {
        #region Internal
        public INavigationService _navigationService;
        public IDialogService _dialogService;
        public IApplicationInstance _applicationInstance; 
        #endregion

        public BaseSessionViewModel(
            INavigationService navigationService,
            IDialogService dialogService,
            IApplicationInstance applicationInstance
            )
        {
            this._navigationService = navigationService;
            this._dialogService = dialogService;
            this._applicationInstance = applicationInstance;
        }
    }
}
