﻿using GoDeliverix.Cliente.Models.Navigation;
using GoDeliverix.Cliente.Services.Shared;
using GoDeliverix.Cliente.Utils;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace GoDeliverix.Cliente.ViewModels.Layout
{
    public class NavigationMasterPageViewModel : BindableBase
    {
        #region Internal
        private INavigationService _navigationService;
        private IEventAggregator _ea;
        #endregion

        #region Properties
        private ObservableCollection<NavigationItem> _navigationItems;
        public ObservableCollection<NavigationItem> navigationItems
        {
            get
            {
                return this._navigationItems;
            }
            set
            {
                SetProperty(ref this._navigationItems, value);
            }
        }
        #endregion

        public NavigationMasterPageViewModel(INavigationService navigationService, IEventAggregator ea)
        {
            this._navigationService = navigationService;
            this._ea = ea;

            this.navigationItems = new ObservableCollection<NavigationItem>();
            this.navigationItems.Add(new NavigationItem() { Icon = IconFont.Home, PageName = "HomePage", Title = "Inicio" });

            this.OnNavigateCommand = new DelegateCommand<string>(this.NavigateAsync);

            this._ea.GetEvent<NavigationServiceEvent>().Subscribe(this.RefreshNavigationItems);
        }

        #region Command
        public DelegateCommand<string> OnNavigateCommand { get; set; }
        #endregion

        #region Implementation
        private async void NavigateAsync(string page)
        {
            await this._navigationService.NavigateAsync(page);
        }

        private void RefreshNavigationItems(IEnumerable<NavigationItem> items)
        {
            this.navigationItems = new ObservableCollection<NavigationItem>(items);
        }
        #endregion
    }
}
