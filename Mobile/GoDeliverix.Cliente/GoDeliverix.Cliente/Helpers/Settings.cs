﻿using GoDeliverix.Cliente.Models.LocalStorage;
using Newtonsoft.Json;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;

namespace GoDeliverix.Cliente.Helpers
{
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants
        private static readonly string EmptyValue = string.Empty;

        private const string SettingsKey = "settings_key";
        private const string UserGuidKey = "user_guid";
        private const string UserSessionKey = "user_session";
        private const string DireccionSessionKey = "direccion_session";

        #endregion


        public static string GeneralSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(SettingsKey, EmptyValue);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SettingsKey, value);
            }
        }

        #region Get
        public static Guid UserGuid
        {
            get
            {
                string value = AppSettings.GetValueOrDefault(UserGuidKey, EmptyValue);
                Guid result = Guid.Empty;
                if (Guid.TryParse(value, out result))
                {
                    result = new Guid(value);
                }
                return result;
            }
            set
            {
                AppSettings.AddOrUpdateValue(UserGuidKey, value);
            }
        }

        public static UserSession UserSession
        {
            get
            {
                string jObject = AppSettings.GetValueOrDefault(UserSessionKey, EmptyValue);
                return string.IsNullOrEmpty(jObject) ? null : JsonConvert.DeserializeObject<UserSession>(jObject);
            }
            set
            {
                string jObject = JsonConvert.SerializeObject(value);
                AppSettings.AddOrUpdateValue(UserSessionKey, jObject);
            }
        }

        public static DireccionSession DireccionSession
        {
            get
            {
                string jObject = AppSettings.GetValueOrDefault(DireccionSessionKey, EmptyValue);
                return string.IsNullOrEmpty(jObject) ? null : JsonConvert.DeserializeObject<DireccionSession>(jObject);
            }
            set
            {
                string jObject = JsonConvert.SerializeObject(value);
                AppSettings.AddOrUpdateValue(DireccionSessionKey, jObject);
            }
        }
        #endregion

    }
}
