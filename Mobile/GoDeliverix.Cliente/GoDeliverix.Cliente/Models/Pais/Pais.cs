﻿using System;
using Newtonsoft.Json;

namespace GoDeliverix.Cliente.Models.Pais
{
    public class Pais
    {
        [JsonProperty("UidPais")]
        public Guid Id { get; set; }

        [JsonProperty("Nombre")]
        public string Nombre { get; set; }
    }
}
