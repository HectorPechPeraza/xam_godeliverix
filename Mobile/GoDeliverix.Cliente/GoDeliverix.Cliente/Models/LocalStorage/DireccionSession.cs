﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoDeliverix.Cliente.Models.LocalStorage
{
    public class DireccionSession
    {
        public Guid Uid { get; set; }

        public Guid UidPais { get; set; }
        public string Pais { get; set; }

        public Guid UidCiudad { get; set; }
        public string Ciudad { get; set; }

        public Guid UidEstado { get; set; }
        public string Estado { get; set; }

        public Guid UidMunicipio { get; set; }
        public string Municipio { get; set; }

        public Guid UidColonia { get; set; }
        public string Colonia { get; set; }

        public string Direccion { get; set; }

        public double Latitude;

        public double Longitude;
    }
}
