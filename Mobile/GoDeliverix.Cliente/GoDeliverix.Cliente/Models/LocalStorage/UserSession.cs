﻿using GoDeliverix.Cliente.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace GoDeliverix.Cliente.Models.LocalStorage
{
    public class UserSession : BaseEntity
    {
        public string Name { get; set; }

        public string Token { get; set; }
    }
}
