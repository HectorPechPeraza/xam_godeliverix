﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace GoDeliverix.Cliente.Models.Direccion
{
    /// <summary>
    /// Contiene los datos de la direccion a partir del catalogo del back end
    /// </summary>
    public class DireccionCatalogo
    {
        [JsonProperty("UidColonia")]
        public Guid UidColonia;

        [JsonProperty("Colonia")]
        public string Colonia;

        [JsonProperty("UidCiudad")]
        public Guid UidCiudad;

        [JsonProperty("Ciudad")]
        public string Ciudad;

        [JsonProperty("UidMunicipio")]
        public Guid UidMunicipio;

        [JsonProperty("Municipio")]
        public string Municipio;

        [JsonProperty("UidEstado")]
        public Guid UidEstado;

        [JsonProperty("Estado")]
        public string Estado;

        [JsonProperty("UidPais")]
        public Guid UidPais;

        [JsonProperty("Pais")]
        public string Pais;

        [JsonProperty("Direccion")]
        public string Direccion;

        public double Latitude;
        public double Longitude;
    }
}
