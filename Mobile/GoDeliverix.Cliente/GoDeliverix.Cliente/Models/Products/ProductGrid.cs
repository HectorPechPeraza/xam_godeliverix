﻿using GoDeliverix.Cliente.Models.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace GoDeliverix.Cliente.Models.Products
{
    public class ProductGrid : BaseEntity
    {
        public string Company { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }
    }
}
