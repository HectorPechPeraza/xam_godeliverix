﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoDeliverix.Cliente.Models.Common
{
    public class BaseEntity
    {
        public Guid Uid { get; set; }
        public string StrUid => this.Uid.ToString();
    }
}
