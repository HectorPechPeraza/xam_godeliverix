﻿using GoDeliverix.Cliente.Utils;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace GoDeliverix.Cliente.Models.Common
{
    public class Response
    {
        public ResponseStatus Status { get; set; }

        public object Result { get; set; }
    }
}
