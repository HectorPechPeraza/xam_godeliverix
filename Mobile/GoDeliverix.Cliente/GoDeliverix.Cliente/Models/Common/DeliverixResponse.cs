﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoDeliverix.Cliente.Models.Common
{
    public class DeliverixResponse
    {
        public string Message { get; set; }

        public bool Status { get; set; }

        public object Data { get; set; }
    }
}
