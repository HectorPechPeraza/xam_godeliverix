﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoDeliverix.Cliente.Models.Navigation
{
    public class NavigationItem
    {
        public string Icon { get; set; }

        public string PageName { get; set; }

        public string Title { get; set; }
    }
}
