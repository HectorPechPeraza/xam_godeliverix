﻿using Prism;
using Prism.Ioc;
using GoDeliverix.Cliente.ViewModels;
using GoDeliverix.Cliente.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using GoDeliverix.Cliente.Views.Auth.Login;
using GoDeliverix.Cliente.ViewModels.Auth;
using GoDeliverix.Cliente.Views.Layout;
using GoDeliverix.Cliente.ViewModels.Layout;
using GoDeliverix.Cliente.Views.Home;
using GoDeliverix.Cliente.ViewModels.Home;
using GoDeliverix.Cliente.ViewModels.Account;
using GoDeliverix.Cliente.Views.Account;
using GoDeliverix.Cliente.Views.Cart;
using GoDeliverix.Cliente.ViewModels.Cart;
using GoDeliverix.Cliente.Views.History;
using GoDeliverix.Cliente.ViewModels.History;
using GoDeliverix.Cliente.Views.Product;
using GoDeliverix.Cliente.ViewModels.Product;
using GoDeliverix.Cliente.Views.SplashScreen;
using GoDeliverix.Cliente.Views.Location;
using GoDeliverix.Cliente.ViewModels.Location;
using GoDeliverix.Cliente.Views.Welcome;
using GoDeliverix.Cliente.ViewModels.Welcome;
using GoDeliverix.Cliente.Views.Auth.SignIn;
using GoDeliverix.Cliente.Views.Dialogs.Error;
using GoDeliverix.Cliente.ViewModels.Dialogs.Error;
using GoDeliverix.Cliente.Services.Dependency.Abstract;
using GoDeliverix.Cliente.Services.Dependency.Implementation;
using GoDeliverix.Cliente.Views.Layout.Error;
using GoDeliverix.Cliente.Views.Dialogs;
using GoDeliverix.Cliente.ViewModels.Dialogs;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace GoDeliverix.Cliente
{
    public partial class App
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();

            await NavigationService.NavigateAsync($"{nameof(SplashPage)}");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<NavigationTabbedPage, NavigationTabbedPageViewModel>();

            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();

            containerRegistry.RegisterForNavigation<HomePage, HomePageViewModel>();

            containerRegistry.RegisterForNavigation<AccountPage, AccountPageViewModel>();
            containerRegistry.RegisterForNavigation<CartPage, CartPageViewModel>();
            containerRegistry.RegisterForNavigation<HistoryPage, HistoryPageViewModel>();
            containerRegistry.RegisterForNavigation<ProductDetailPage, ProductDetailPageViewModel>();

            containerRegistry.RegisterForNavigation<SplashPage, SplashPageViewModel>();
            containerRegistry.RegisterForNavigation<ChooseLocationPage, ChooseLocationPageViewModel>();
            containerRegistry.RegisterForNavigation<WelcomePage, WelcomePageViewModel>();

            containerRegistry.RegisterForNavigation<SignInStepOnePage>();
            containerRegistry.RegisterForNavigation<MyLocationsPage, MyLocationsPageViewModel>();

            // Error Pages
            containerRegistry.RegisterForNavigation<NotAvailablePage>();

            // Dialogs
            containerRegistry.RegisterDialog<NetworkErrorDialog, NetworkErrorDialogViewModel>();
            containerRegistry.RegisterDialog<ProductFilterDialog, ProductFilterDialogViewModel>();

            // Singleton
            containerRegistry.RegisterSingleton<IApplicationInstance, ApplicationInstance>();
        }
    }
}
