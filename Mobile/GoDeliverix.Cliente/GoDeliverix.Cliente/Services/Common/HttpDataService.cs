﻿using GoDeliverix.Cliente.Models.Common;
using GoDeliverix.Cliente.Utils;
using GoDeliverix.Cliente.Utils.Constants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace GoDeliverix.Cliente.Services.Common
{
    public class HttpDataService
    {
        private HttpClient _httpClient;
        private string _BaseUrl;
        private string _Token;

        /// <summary>
        /// Service to make HTTP Request
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="requiresToken"></param>
        /// <param name="token"></param>
        public HttpDataService(string controller, bool requiresToken = false, string token = "")
        {
            this._BaseUrl = $"{ApplicationVariables.BaseUrl}/{controller}/";
            this._Token = token;

            this._httpClient = new HttpClient();
            this._httpClient.BaseAddress = new Uri(this._BaseUrl);
            this._httpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            if (requiresToken)
            {
                this._httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", this._Token);
            }
        }

        /// <summary>
        /// Make a http request of type GET
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="action"></param>
        /// <param name="responseType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<Response> GetAsync<T>(string action, HttpResponseType responseType, IDictionary<string, string> parameters = null)
        {
            if (!this.ValidateNetworkAccess())
            {
                return new Response() { Status = ResponseStatus.NetworkError, Result = null };
            }

            string urlParams = parameters == null ? "" : ConvertStringArguments(parameters);

            var response = await this._httpClient.GetAsync(action + urlParams);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Response result = new Response();
                result.Status = ResponseStatus.Success;

                string content = await response.Content.ReadAsStringAsync();
                switch (responseType)
                {
                    case HttpResponseType.Object:
                        result.Result = JsonConvert.DeserializeObject<T>(content);
                        break;
                    case HttpResponseType.List:
                        result.Result = JsonConvert.DeserializeObject<IEnumerable<T>>(content);
                        break;
                    case HttpResponseType.None:
                        result.Result = "Success";
                        break;
                    default:
                        break;
                }
                return result;
            }

            return new Response() { Status = ResponseStatus.Failed, Result = null };
        }

        /// <summary>
        /// Make a http request of type POST
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="action"></param>
        /// <param name="resource"></param>
        /// <param name="responseType"></param>
        /// <returns></returns>
        public async Task<Response> PostAsync<T>(string action, string resource, HttpResponseType responseType)
        {
            if (!this.ValidateNetworkAccess())
            {
                return new Response() { Status = ResponseStatus.NetworkError, Result = null };
            }

            StringContent contentRequest = new StringContent(resource, Encoding.UTF8, "application/json");
            var response = await this._httpClient.PostAsync(action, contentRequest);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Response result = new Response();
                result.Status = ResponseStatus.Success;

                string content = await response.Content.ReadAsStringAsync();
                switch (responseType)
                {
                    case HttpResponseType.Object:
                        result.Result = JsonConvert.DeserializeObject<T>(content);
                        break;
                    case HttpResponseType.List:
                        result.Result = JsonConvert.DeserializeObject<IEnumerable<T>>(content);
                        break;
                    default:
                        break;
                }
                return result;
            }

            return new Response() { Status = ResponseStatus.Failed, Result = null };
        }

        public async Task<Response> PutAsync<T>(string action, HttpResponseType responseType)
        {
            return new Response() { Status = ResponseStatus.NetworkError, Result = null };
        }

        public async Task<Response> DeleteAsync<T>(string action, HttpResponseType responseType)
        {
            return new Response() { Status = ResponseStatus.NetworkError, Result = null };
        }

        public bool ValidateNetworkAccess()
        {
            bool result = false;
            var current = Connectivity.NetworkAccess;

            switch (current)
            {
                case NetworkAccess.Internet:
                    // Connected to internet
                    result = true;
                    break;
                case NetworkAccess.Local:
                    // Only local network access
                    result = true;
                    break;
                case NetworkAccess.ConstrainedInternet:
                    // Connected, but limited internet access such as behind a network login page
                    result = true;
                    break;
                case NetworkAccess.None:
                    // No internet available
                    result = false;
                    break;
                case NetworkAccess.Unknown:
                    // Internet access is unknown
                    result = false;
                    break;
            }
            return result;
        }
        private string ConvertStringArguments(IDictionary<string, string> arguments)
        {
            string urlArguments = string.Empty;
            foreach (var arg in arguments)
            {
                if (urlArguments == string.Empty)
                    urlArguments = arg.Value == string.Empty ? string.Empty : $"?{arg.Key}={arg.Value}";
                else
                    urlArguments += arg.Value == string.Empty ? string.Empty : $"&{arg.Key}={arg.Value}";
            }
            return urlArguments;
        }
    }
}
