﻿using GoDeliverix.Cliente.Models.Direccion;
using GoDeliverix.Cliente.Services.Common;
using GoDeliverix.Cliente.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GoDeliverix.Cliente.Services
{
    public class LocationService
    {
        #region Properties
        protected HttpDataService _httpService;
        #endregion

        public LocationService()
        {
            this._httpService = new HttpDataService("Location");
        }

        /// <summary>
        /// Make a simple http request to the server to check availability
        /// </summary>
        /// <returns></returns>
        public async Task<bool> ValidateServiceAvailability()
        {
            var response = await this._httpService.GetAsync<string>("CheckAvailability", HttpResponseType.None);
            return response.Status == ResponseStatus.Success ? true : false;
        }

        /// <summary>
        /// Returns location data according to geolocation
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <returns></returns>
        public async Task<DireccionCatalogo> GetAddressByGeolocation(double latitude, double longitude)
        {
            IDictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("latitude", latitude.ToString());
            parameters.Add("longitude", longitude.ToString());

            var response = await this._httpService.GetAsync<DireccionCatalogo>("GetAddressByGeolocation", HttpResponseType.Object, parameters);
            if (response.Status == ResponseStatus.Success)
            {
                return (DireccionCatalogo)response.Result;
            }

            return null;
        }

    }
}
