﻿using GoDeliverix.Cliente.Models.Navigation;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace GoDeliverix.Cliente.Services.Shared
{
    public class NavigationServiceEvent : PubSubEvent<IEnumerable<NavigationItem>>
    {
    }
}
